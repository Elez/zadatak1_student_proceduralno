<!DOCTYPE html>
<?php 
	session_start();
?>
<html>
    <head>
	<title>Student</title>
    </head>
    <body>

	<style>
            .error {color:red}
            input.error{border:1px solid red; color: black}
	</style>

		<!--  Wrapper begins -->

	<div class="Wrapper"> 

            <h1 align="center">Student</h1>

		<!-- Form_student begins -->

            <?php // cuvanje vrednosti iz prve forme u sesiji
        
		$ime = null;
		$prezime = null;
		$index = null;

		$error['student']['error'] = array(
			'$firstname_error' => "",
			'$lastname_error' => "",
			'$index_error' => "",
			'$exam_error' => "",
			'$mark_error' => "",
			); 	

		if(isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['town'])){
                    
			$_SESSION['student']['firstname'] = $_POST['firstname']; 
			$_SESSION['student']['lastname']  = $_POST['lastname'];  
			$_SESSION['student']['index']  = $_POST['index'];   
			$_SESSION['student']['town'] = $_POST['town'];    
                        $_SESSION['student']['ispiti']=null;     // postavlja vrednost ispti iz sesije na null;					
				                                  // tu ce se cuvati vrednosti iz druge forme
			//}
			//dodela promenljivih moze i ne mora da se koristi
			/*if (isset($_SESSION['student'])) {

                            $ime = $_SESSION['student']['firstname'];
                            $prezime = $_SESSION['student']['lastname'];
                            $index = $_SESSION['student']['index'];

                            // dodela vrednosti iz sesije promenljivim koje ubacujemo u value atribut
			}*/
			//if(isset($_SESSION['student']['firstname']) && isset($_SESSION['student']['lastname']) && isset($_SESSION['student']['index'])){
					
			if (empty($_SESSION['student']['firstname'])) {
                            $error['error']['firstname_error'] = "Potrebno je uneti ime";
			}
			if (empty($_SESSION['student']['lastname'])) {
                            $error['error']['lastname_error'] = "Potrebno je uneti prezime";
			}
			if (empty($_SESSION['student']['index'])) {
                            $error['error']['index_error'] = "Potrebno je uneti index";
			}	
		}
            ?>

	<div class="Form" style="margin-top:20px; margin-left:10px"> 

            <form style="margin-bottom:5px" method="post">

		Ime:<br>

		<input type="text" name="firstname" <?php //echo isset($error['error']['firstname_error'])?'class="error"':''; ?>
				  					   value= <?php echo $_SESSION['student']['firstname'] ?>  >
		<span class="error" >* <?php echo isset($error['error']['firstname_error'])?$error['error']['firstname_error']:'';?></span>

		<br>Prezime:<br>

		<input type="text" name="lastname"  <?php //echo isset($error['error']['lastname_error'])?'class="error"':''; ?>
				  					   value= <?php echo $_SESSION['student']['lastname'] ?>  >
		<span class="error" >* <?php echo isset($error['error']['lastname_error'])?$error['error']['lastname_error']:'';?></span>

		<br>Broj indeksa:<br>

		<input type="text" name="index"  <?php //echo isset($error['error']['index_error'])?'class="error"':''; ?>
				   value= <?php echo $_SESSION['student']['index']  ?>  >

		<span class="error" >* <?php echo isset($error['error']['index_error'])?$error['error']['index_error']:'';?></span>	

		<br>Mesto:<br>

		<select name="town">
					
                    <option name = "beograd" value="beograd" <?php if(isset($_SESSION['student']) && ($_SESSION['student']['town'] == "beograd")) echo 'selected' ?> >Beograd</option>
                    <option name = "nis" value="nis" <?php if(isset($_SESSION['student']) && ($_SESSION['student']['town'] == "nis")) echo 'selected' ?> >Nis</option>
                    <option name = "novi sad" value="novi sad" <?php if(isset($_SESSION['student']) && ($_SESSION['student']['town'] == "novi sad")) echo 'selected' ?> >Novi Sad</option>
					
                </select>
		<br>

		<input type="submit" value="Sacuvaj" name="submituj">

            </form>
				
	</div>

	<!-- Form_student ends -->

	<?php // pravljenje reda za tabelu
			
            $red = null;
            if (isset($_SESSION['student'])) {
		$red = array();
		if (isset($_SESSION['student']['ispiti'])) {
                    $red = $_SESSION['student']['ispiti'];
		}

		if ( (isset($_POST['exam']) && isset($_POST['mark'])) && (!empty($_POST['exam']) &&  !empty($_POST['mark']))) {

		// provera da li su polja prazna moze i da se skloni s obzirom na required atribut

                    $red[] = array(
			'exam' =>$_POST['exam'],
			'mark' => $_POST['mark'] 
			);
		}
		$_SESSION['student']['ispiti'] = $red;
            }
	?>

	<!-- Table begins -->

	<div  style="margin-top:25px; margin-left:10px" >
            <table id="myTable" border="1px solid black" method="post">
		<tr>
                    <th>Br.</th>
                    <th>Ispit</th>    
                    <th>Ocena</th>				     
                </tr>

            <?php 

		for ($i=0; $i < count($red); $i++) { //prolazim kroz niz redova
                    $br = $i + 1;                    //promenljiva koja predstavlja redni broj u tabeli
                    echo "<tr><td> $br </td>";       //ispisujem prvo polje u prvom redu(sadrzi za sad samo redni broj)
                    foreach ($red[$i] as $r) {         
                    echo "<td> $r </td>";        //ispisujem vrednosti u prvom redu tabele, ostala 2 polja(exam, mark)
                    }
                    echo "</tr>";
                    }
            ?>
            </table>
            <br>
	</div>	

	<!-- Table ends -->

	<!-- Form_exam begins -->			

	<div class="Form_exam"  style="margin-top:25px; margin-left:10px" >

            <form method="post">

		Ispit:<br>

		<input type="text" name = "exam" >

		<br>Ocena:<br>

		<input type="number" name = "mark" min = "6" max = "10" >

		<br><br>

		<input type="submit" value="sacuvaj">	

            </form>

	</div>

	<!-- Form_exam ends -->
	
    </div> 

    <!-- Wrapper ends -->

    </body>
</html>